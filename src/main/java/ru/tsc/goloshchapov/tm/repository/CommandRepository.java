package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.constant.ArgumentConst;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of commands"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application"
    );

    public static final Command[] COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }

}
